%
% Data Transformation
%
\subsection{Data Transformations}

%
% General
% infrastructure
The majority of source code is implemented in PostgreSQL 9.6.9 (Postgres) because
it is the primary support for the MIMIC database and allows the community to
reproduce our work on limited resources without licensing costs and benefit
from recent Postgres improvements in the data processing area. Some elaborated
data transformations have been implemented as Postgres functions.

% OMOP version
The OMOP CDM version 5.3.3.1 (OMOP) tables were created from the provided
scripts with some changes documented in our scripts. OMOP defines 15
standardized \textsc{clinical} data tables, 3 \textsc{health} system data
tables, 2 \textsc{health economics} data tables, 5 tables for \textsc{derived}
elements and 12 tables for standardized \textsc{vocabulary}. The
\textsc{vocabulary} tables were loaded from concepts downloaded from Athena \footnote{https://www.ohdsi.org/analytic-tools/athena-standardized-vocabularies/} and
the clinical and derived tables were loaded from MIMIC.

% MIMIC version
MIMIC-III version 1.4.21 (MIMIC) was also loaded into Postgres with the provided
scripts. A subset of 100 patients over the 46 000 total MIMIC patients selected
based on their broad representativeness in the database and cloned into a
second instance to serve as a light and representative development set. \\


% Structural Mapping 
% plan:
% evaluation : dispatching, domains, relationships, statistics
% loss of information, modification of the structure
\subsubsection{Structural Mapping}
The structural mapping aims at moving the MIMIC data into the right place in
OMOP with some data transformations. It parts into three phases: conception,
implementation and evaluation.

% conception step
The \textbf{conception} phase consists of looping over each MIMIC table and
choosing an equivalent location in OMOP for each column. In general both
projects were appropriately documented but in several cases, we needed
some clarification from MIMIC contributors on the dedicated MIMIC git
repository\footnote{https://github.com/MIT-LCP/mimic-code}, or from the OMOP
community forum\footnote{http://forums.ohdsi.org/}. Some trickier choices have
been discussed in the MIMIC-OMOP git
repository\footnote{https://github.com/MIT-LCP/mimic-omop} and can be tracked
in the commit logs.

% implementation step

The \textbf{implementation} is done by an Extract-Transform-Load (ETL) process
which is composed of Postgres scripts, extracting information from the source or
concept mapping tables, then transforming and loading an OMOP target table. The
scripts are managed sequentially through a main program. 
% modifications of the OMOP structure
In last resort some modification of the structural model of OMOP have
been made. A dedicated script recaps all of them. It contains columns name
modifications, new columns, columns type modifications or database indexing
modification.
% the sequence
In particular, each source table has been added a global unique sequence
incremented from 0 that serves as the primary key and links in the OMOP target
tables. As a result every record is uniquely identified allowing to chain the
information with OMOP while simplifying the primary/foreign key maintenance.


Although \textbf{evaluating} a structural model is difficult \cite{moody-quality}, 
several articles have attempted to assess the quality of the CDM
\cite{kahn-data-2012,omop-vs-pcornet}. The criteria developed by Khan et
al \cite{kahn-quality}, which refer to the  Moody and Shanks metrics
\cite{moody-quality}, have been adapted to assess the quality of the data
transformation (table \ref{table:quality}). 

\begin{table*}[t]
%\tbl{Data Transformation Quality Evaluation Metrics}{%
\caption{Transformation Quality Evaluation Metrics}
\begin{tabular}{lp{12cm}}\toprule
Data Model Dimension              & Descriptions                                                                                                      \\\colrule
Completeness - structural mapping & Domain coverage : coverage of sources domains that are accommodated by the standard OMOP model                    \\
Completeness - conceptual mapping & Data coverage : coverage of sources data concepts that mapped to standard OMOP concept                            \\
Integrity                         & "Meaningful data relationships and constraints that uphold the intent of the data's original purpose" \cite{kahn-quality} \\
Flexibility                       & The ease to expand the standard model for new datatypes, concepts                                                 \\
Integration                       & The capacity of the standard model to use multiples terminology and links its to standard one                     \\
Implementability                  & The stability of the models, the community, the cost of adoption                                                  \\
Understandability                 & The ease of the standard model to be understood                                                           	      \\
Simplicity                        & The ease of querying the standard model - the model should contains the minimum of concepts and relationship      \\\botrule 
\end{tabular}
\label{table:quality}
\end{table*}
\normalsize


% evaluation step 
Beside Moody and Shanks, we provide a set of controls to guaranty a correct transformation.
In order to compare overall statistics, some SQL queries have been setup to
compare MIMIC and MIMIC-OMOP and we provide basic populations characterizations.
All tables have been covered and tested through simple counts, aggregate
counts or distribution checks.
% statistics evaluation of loss
We estimated the loss of information during the ETL process by measuring the 
percentage of both columns and rows lost in the process as other previous studies 
have done \cite{omop-nashville}.
This is important to note we have chosen not to keep irrelevant informations:
for example some rows are known to be invalid in MIMIC or some informations are
redundant.
Each ETL script has been tested using pgTAP, a unit test framework for Postgres.
Each unit test script checked whether a particular OMOP target table was loaded
correctly. 
% Integrity
Integrity constraints (primary keys, foreign keys, non-nullable
columns) have been included to apply integrity checks at ETL runtime. 
% Achilles
The last axe of the structural evaluation is Achilles Sofware. It is an
open-source analysis software produced by OHDSI\footnote{https://www.ohdsi.org/web/achilles/}. 
Like many previous authors, we used the Achilles to assess data quality 
\cite{achilles-evaluation}.  
This tool is used for data characterization, data quality assessment (Achilles' heel) and
health observation data visualization. It has been common practice to
perform Achilles tests and use it as a quality assessment in related studies. All
the resulting tables are presented in the results section. 
\\


% Conceptual Mapping 
\subsubsection{Conceptual Mapping}
The conceptual mapping aims at aligning the MIMIC local terminologies
to the OMOP's standard ones. It consists of three phases: integration, alignment and
evaluation.\\
% integration phase
The \textbf{integration} phase is about loading both kind of terminologies into the OMOP
vocabulary tables.
% Athena codes
The OMOP terminologies are provided by the Athena tool and were loaded with the
associated programs. We have used an export with all terminologies without
licensing limitations.
% MIMIC local codes
The local terminologies have been extracted from the multiple MIMIC tables and
loaded in the OMOP \textsc{concept} table. When possible, relevant informations from the
original MIMIC tables have been concatenated in the \textit{concept\_name}
column. 
MIMIC local concepts were loaded  with a
\textit{concept\_id} identifier starting from 2 billion (lower numbers are
reserved for OMOP terminologies\footnote{http://www.ohdsi.org/web/ wiki/doku.php?id=documentation:cdm:concept}). 
In the OMOP \textsc{concept} table, MIMIC concepts can be distinguished with the
\textit{vocabulary\_id} identifier equal to "MIMIC code" and a
\textit{domain\_id} identifier targeting the OMOP table in which the
corresponding data is stored. This domain information is used in the ETL
to send the information in the proper table. We want to call this method
"concept-driven dispatching".
OMOP documentation explains that conceptual mapping has to be done before the
structural mapping because the nature of the OMOP standard concepts guides in
which table (domain) the information should be stored. The concept-driven
dispatching methodology enable changing the concept mapping after the
transformation without modifying the underlying ETL code because the latter is
dynamically based on the \textsc{concept} table content.

%alignment phase
The \textbf{alignment} phase to standardizing local MIMIC codes into OMOP standard
codes, had four distinct cases. In the \emph{first} case, some MIMIC data is by chance
already coded according to OMOP standard terminologies (e.g. LOINC laboratory results) and,
therefore, the standard and local concepts are the same. 
In the \emph{second}
case, MIMIC data is not coded in the standard OMOP terminologies, but the mapping is already
provided by OMOP (ex: ICD9/SNOMED-CT), so the domain tables have been loaded
accordingly.  
In the \emph{third} case, terminology mapping is not provided, but it is
small enough to be done manually in a few hours (such as demographic status,
signs and symptoms). 
In the \emph{fourth} case, terminology mapping is not provided and consists of
a large set of local terms (admission diagnosis, drugs). Then, only a subset of
the most represented codes was manually mapped.
% fuzzy match
When a manual terminology mapping concept is required, a mapping csv file has been
built. This solution can be adapted to medical users who do not have training
in database engineering. The spreadsheet has several columns such as
local/standard labels, ids and also comments, evaluation metrics and a script
loads them into the Postgres when completed. 
We have chosen to use simple SQL queries that are flexible enough to be queried on
demand or to generate a pre-filled csv with the best matches. It uses Postgres
full-text ranking features and links local and standard candidates with a
rating function based on their labels. This work was performed under the control
of an intensivist.

% evaluation
The \textbf{evaluation} phase was both quantitative and qualitative.
The quantitative evaluation measures the completeness of our work : the percent 
of concepts that are mapped to a standard.
The qualitative evaluation evaluates the correctness. For newly generated mappings 
it has consisted of manually tagging each mapping with a score between 0 and 1 
and eventually write a commentary on each mapped concept. 
In case the mapping of was provided by OMOP - automatic OMOP terminologies mapping -,
 the evaluation was performed on a subset of concepts manually picked within each terminology.

%
%
% Analytics
%
\subsection{Data Analytics}
Beyond the model transformation and respect of the OMOP standardisation process
we applied some analysis.

% scores
MIMIC provides a large number of SQL scripts for preprocess and normalize data,
calculate derived scores and defined cohorts as known as "contrib". Some of
them have been implemented on top of the OMOP format to load the OMOP derived
tables. 

% denormalized tables
A set of \emph{general denormalized} tables has been built on top of the
original OMOP format that have the \textit{concept\_name} related to the
\textit{concept\_id} columns. The \textsc{concept} table is a central element
of OMOP and, therefore, it is involved in many joins to obtain the concept
label. By precalculating the joins with the \textsc{concept} tables, the
denormalized tables faster calculation and simplify SQL queries.
% Lisibilité améliorée, 

% specialized materialized views
In addition, a set of \emph{specialized analytical tables} has been built on
the original OMOP format. The OMOP \textsc{microbiologicalevents} table is a
reorganization of the \textsc{measurement} table data of microorganisms and
associated susceptibility testing antibiotics and is based on the MIMIC
\textsc{microbiologicalevents} table.  The OMOP \textsc{icustays} table allows
to quickly obtain the patients admitted in resuscitation and is inspired by the
MIMIC \textsc{icustays} tables.

% note nlp
The OMOP \textsc{note\_nlp} table was originally designed to store final or intermediate 
derived informations and metadata from clinical notes. When definitive, the 
extracted information is intended to be moved to the dedicated domain or table 
and then reused as regular structured data.
When the information is still intermediate, it is stored in the \textsc{note\_nlp} 
table and can be used for later analysis. 
To populate this table, we provided two information extraction pipelines. 
The \emph{first} pipeline extracted numerical values such as weight, height, 
body mass index and left ventricular cardiac ejection fraction from medical notes 
with a SQL script. The resulting structured numerical values were loaded into the 
measurement or observation tables according to their domain. 
The \emph{second} pipeline \emph{section extractor} based on the apache UIMA 
framework divides notes into sections to help analysts choose or avoid certain 
sections of their analysis.
Section templates (such as "Illness History") have been automatically extracted
from text with regular expressions, then filtered to keep only the most
frequent (frequency > to 1\%). 


% datathon
A 48-hour open access datathon\footnote{http://blogs.aphp.fr/dat-icu/} was set up 
in Paris AP-HP (Assistance Publique des Hopitaux de Paris) in collaboration with 
the MIT once the MIMIC-OMOP transformation was 
ready for research. This datathon was organised to evaluate OMOP as an
alternative data model for accessing and analysing MIMIC data during  a real
event. Scientific questions had been prepared in an online forum where
participants could introduce themselves and propose a topic or choose an
existing one. 
OMOP has been loaded into apache HIVE 1.2.1 in ORC format. Users had access to 
the ORC dataset from a web interface jupyter notebooks with python, R or scala. 
A SQL web client allowed teams to write SQL from presto to the same dataset. 
The hadoop cluster was based on 5 computers with 16 cores and 220GB of RAM memory. 
The MIMIC-OMOP dataset has been loaded from a Postgres instance to HIVE thought apache 
SQOOP 1.4.6 directly in ORC format. Participants also had access to the Schemaspy 
database physical model to access the OMOP physical data model with both 
table/column comments and key primary/foreign relationships materializing the 
relationships between the tables. All queries have been logged.

