%
% 1. ICU and BIGDATA
% 2. MIMIC DB & its limitations
% 3. CDM can overcome this limitations (and are better than FHIR or I2B2 thanks to standard concepts)
% 4. OMOP is better
% 5. Evaluation method and importance of the work
%    Objective threefold are: 1. transform, 2. contribute 3. analysis
%
% ICU and BIGDATA
Intensive care units (ICUs) are designed to provide comprehensive support 
to the most severely-ill patients within a hospital \cite{angus2000}.
Mortality is typically high among these patients, both during and after the
hospital stay \cite{icu-mortality}. Understanding the effectiveness of interventions 
on patient outcomes remains a challenge, due to heterogeneity of patients, 
complexity of disease, and variation in care patterns.
Intensivists use a limited level of evidence to guide decision making \cite{icu-evidence} 
whereas ICUs are a high density environment for data production. 

With the increasing adoption of electronic health record (EHR) systems around
the world leading to large amounts of clinical data \cite{bigdata-promise} and
the development of data mining, innovation throught health data is likely to
play an important role in clinical medicine \cite{bigdata-mining}. 
Indeed, based on important medical informations, expectations are to improve
clinical outcomes and practices, enable personalized medicine and guide early
warning systems, and also easily enroll a large, multicenter cohort while
minimizing costs.

% MIMIC DB
% - local terminologies
% - one center
% - two successive critical care information systems (CCI)
% - a model that reflect the EHR

% MIMIC-i:  1996
% MIMIC-ii: 2002
% MIMIC-iii: 

\emph{MIMIC-III} (Medical Information Mart for Intensive Care) is a high
granularity dataset of over 60,000 intensive care stays and 46 000 unique
patients from two successive ICU systems at the Beth Israel Deaconess Medical
Center in Boston admitted from 2001 to 2012. 
It is the first ICU database available for free and has been intensively used
in research resulting in more than 300 international publications. 
However, its monocentric nature makes it difficult to generalize findings to other ICUs. 
The MIMIC relational data model reflects the original intensive care information systems, 
as evidenced by the two separate \textsc{inputevent\_mv} and \textsc{ouputevent\_cv} \cite{mimic-nature}
or the two separate terminologies for physiological data. 
This leads analysts (datascientists, statisticians, etc.) 
to reconcile the corresponding data to address this heterogeneity during the 
 pre-processing step of each study.

% Why a Common Data Models

For Kahn et al. \cite{kahn-data-2012}, "databases modelling is the process 
of determining how data are to be stored in a database". 
It specifies data types, constraints, relationship and metadata definitions 
and provides a standardized way to represent resources/data and their relationships. 
Some studies have shown that using a common data model (CDM) by standardizing 
the structure (data model) and concepts (terminological model) of the database 
allows large scale multicenter research, exploitation of rare diseases or rare events
and catalyzes research by sharing practices, source code and tools \cite{cdm-review,data-enclave}. 
However, some studies have shown that the results are not fully reproducible 
from one CDM to another \cite{cdm-comparison} or from one centre to another \cite{omop-replicability}. 
Some approaches argue that keeping the local conceptual model \cite{fhir-deep}
and the local structural model \cite{imi-protect} leads to better results. 
On one hand, keeping MIMIC on its specific form 
will not solve the limitation for multicenter research but on the other hand, 
a fully standardized form would introduce other disadvantages, such as loss of data
and lower computational performances.
The ideal solution is probably in between to allow local or standardized
analysis depending on the research question.

% OMOP
\textit{OMOP} (Observational Medical Outcomes Partnership Common Data Model) 
is a CDM originally designed for multicenter research related to adverse drug 
adverse events and now extends to medical, labortory and genomic cases. 
OMOP provides structural and conceptual models relying on reference terminologies
such as SNOMED for diagnostics, RxNORM for drugs and LOINC for laboratory results. 
Several examples of database 
transformed into OMOP have been published \cite{omop-german,omop-nashville} 
and OMOP stores 682 million patient records from around the world \cite{omop-bigboy}. 
Each clinical area is stored in different dedicated tables. 
The OMOP conceptual model is based on a closure table pattern \footnote{https://karwin.blogspot.com/2010/03/rendering-trees-with-closure-tables.html} 
capable of ingesting any simple, hierarchical and also graph terminologies 
such as SNOMED. In addition to local terminologies, OMOP defines
and maintains a set of standard terminologies to be mapped unidirectionally 
(local to standard) by implementers.
Although OMOP has proven its reliability \cite{omop-eval}, 
the concept mapping process is known to have an impact on results \cite{omop-concept-impact} 
and the application of the same protocol on different data sources 
leads to different results \cite{omop-replicability}. 
This shows the importance of keeping local terminologies so that local analysis is
still possible.  Previous preliminary work has been done on the translation of
MIMIC into OMOP \cite{mimic-omop-previous}.  This work remains to be refined
and updated for proper evaluation.

% CDM Comparison

In a recent comparative study of different CDM \cite{cdm-review,omop-vs-pcornet}
OMOP obtained best results for completeness, integrity, 
flexibility, simplicity of integration, implementability, for a wider coverage of
the structural and conceptual model, a more systematic analysis thanks 
to an analytical library and to visualization tools and easier access to data
through SQL queries.
In terms of conceptual approach, OMOP offers a broader set of standard concepts.
In terms of structural CDM it is very rigorous in how data should be loaded 
into specific tables while others CDM such i2b2 are very flexible 
with a general table that solves all data domains. This rigorous approach 
is necessary for standardization. 
Previous work has been done to load MIMIC-III into i2b2 \cite{mimic-i2b2} -
however the work couldn't be finalized due to tricky concept mapping to standard
terminologies tasks. OMOP has the advantage of not making the terminology mapping
step mandatory by keeping the local codes accessible to analysts. Compared to the Fast
Healthcare Interoperability Resources (FHIR) \footnote{https://www.hl7.org/fhir/}, 
OMOP performs better as a
conceptual CDM because the FHIR ressources currently do not specify the
terminology to be used for most of the attributes. 
OMOP relational model can be materialized in csv format
 and stored in any relational database when FHIR uses json files and 
needs some processing and higher skills to exploit.
Among the above models, OMOP is the best candidate to overcome the MIMIC
limitations mentioned earlier.

% Evaluation of MIMIC-OMOP
Our article was guided by the two following dimensions: 
\begin{enumerate}
\item Data Transformations : evaluate the process of transforming MIMIC into OMOP
in terms of time needed, skills required and quality of the result.
\item Data Analytics : evaluate the resulting dataset to support efficient,
shareable and real-time analysis.
\end{enumerate}
