SELECT count(*) as person_count from person;
SELECT count(*) death_count from death;
SELECT count(*) visit_occurrence from visit_occurrence;
SELECT count(*) visit_detail from visit_detail;
SELECT count(*) measurement from measurement;
SELECT count(*) observation from observation;
SELECT count(*) drug_exposure from drug_exposure;
SELECT count(*) procedure_occurrence from procedure_occurrence;
SELECT count(*) condition_occurrence from condition_occurrence;
SELECT count(*) note from note;
SELECT count(*) note_nlp from note_nlp;
SELECT count(*) cohort_attribute from cohort_attribute;
SELECT count(*) care_site from care_site;
SELECT count(*) observation_period from OBSERVATION_PERIOD;
SELECT count(*) provider from PROVIDER;
SELECT count(*) specimen from SPECIMEN;
SELECT count(*) from concept where concept_id > 2001000000;

