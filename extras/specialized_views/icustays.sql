--DROP MATERIALIZED VIEW IF EXISTS SAPSII CASCADE;
--CREATE MATERIALIZED VIEW SAPSII as

-- ajouter
-- age
-- expire in icu
-- expire in hospital
-- los
WITH ICUSTAYS AS
(
	SELECT person_id, visit_occurrence_id, visit_detail_id
	, visit_start_datetime
	FROM visit_detail
	WHERE visit_detail_concept_id = 581382                             -- concept.concept_name = 'Inpatient Intensive Care Facility'
	AND visit_type_concept_id = 2000000006                            -- concept.concept_name = 'Ward and physical location'
),
