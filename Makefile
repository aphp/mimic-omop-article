all:
	xelatex mimic-omop.tex
	bibtex mimic-omop.aux
	xelatex mimic-omop.tex
	xelatex mimic-omop.tex

delete:
	rm -f mimic-omop.pdf
